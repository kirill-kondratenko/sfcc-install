# README #

## What is this repository for? ##

Install scripts for rolling out SFCC\CCMK project on dev machines. Can handle from scratch or update installs.

Warning: will overwrite existing local deployment if using same folder 
***
## How do I get set up?

- pull repository 
- set up config file with your personal data - see default.json for sample data.
	- **bb-cred** - bitbucket credentials. Suggest using login:password format as otherwise it doesn't play nicely with 2FA
	- **sandbox-host** - your sandbox that will be used for cartridge and data uploads, host only, no protocol
	- **sandbox-user** - your MK account email
	- **sandbox-pass** - your password used for authentication with demandware.net 
	- **sandbox-codeversion** - version of code to be uploaded into, conventions is to use your name-surname e.g. kkondrat for Kirill Kondratenko
	- **sandbox-client-id** - SFCC Client ID
	- **sandbox-client-secret** -  restricted: [SFCC Client Secret](https://marykay.atlassian.net/wiki/spaces/MK/pages/249397982/Credentials+-+Test+SFRA#Credentials-Test(SFRA)-SFCCClientIDandClientSecret(Sandboxes+QAEnvironment))
	- **sfra-root** - local install path
	- **skip-X** - flags that allow to bypass install steps (see install-win-ccmk.ps1 for details)
- run "*install-win-ccmk.ps1 your-config.json*"

May not run in full at first run as Powershell for some reason doesn't sync up path variable so sh.exe may not be correctly registered to run. 
Script will obliterate install folder so make sure that used folder is empty or save your changes beforehand to some other location. 

*Sidenote: Incidentally you actually can "git stash" to remote repo, but is looks like a bad idea - [SO: Is git stash stack pushed to the remote repo?](https://stackoverflow.com/questions/23952575/is-git-stash-stack-pushed-to-the-remote-repo)*

Script will require admin privileges to run (not fully tested without admin, may be some issues with restricted folders access)
Script can be run multiple times, you can also configure skipping steps in case you already have part of install completed.

### Sample data

This is sample of config file used for setup automation. You should have:

- valid bitbucket account
- valid SFCC sandbox
- admin privileges on machine (not tested without admin, may not work for all steps)

```javascript
{
	"bb-cred"		: "name-surname:pass@bitbucket.org",
	"sandbox-host" 	: "dev0X-ap0X-marykay.demandware.net",
	"sandbox-user" 	: "name.surname@mkcorp.com",
	"sandbox-pass" 	: "password",
	"sandbox-codeversion" 	: "nsurname",	
	"sandbox-client-id" 	: "6f37458f-6b36-4c97-aedb-129cfc211d05",
	"sandbox-client-secret" : "secret-see-wiki",
	"sfra-root"		: "D:\\ProjectOne\\CCMK",
	"skip-1"  : false,
	"skip-2"  : false,
	"skip-3"  : false,
	"skip-4"  : false,
	"skip-5"  : false
}
```
***
## Reference guides
Following wiki articles describe install and develop process in more detail:

- [Sandbox setup guide](https://marykay.atlassian.net/wiki/spaces/MK/pages/442893426/Developer+Sandbox+Setup+Guide)
- [Developer guide](https://marykay.atlassian.net/wiki/spaces/MK/pages/249397300/Getting+Started+-+Developer+Guide+SFRA)

## Useful links
These are not (easily) automated tools to add. Manuall install is available.

Chrome extensions (Chrome team is actively against [plugin automation](https://stackoverflow.com/questions/16800696/how-install-crx-chrome-extension-via-command-line) as it is prime attack vector for malware) :

- [Demandware With Ease](https://chrome.google.com/webstore/detail/demandware-with-ease/ffhabonelknmejmdnekedmijlhebpcio?hl=en)

***
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

