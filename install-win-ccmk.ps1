<#
.SYNOPSIS
    Generic script to roll out SFCC deployment on Windows machine	
.DESCRIPTION
	Steps:
    1. Installs: VS Code, Chocolatey, Git for Windows, Node.js
	2. Configures: plugins for VS code (Prophet, Beautify, etc), sets VS code user settings
	3. Pulls: sfcc cartridge repo
	4. Runs: npm install && npm compile
	5. Deploys: code and data to sandbox
	
.PARAMETER Config
	Name of config file to be used, if not provided, will prompt to create new config.
.Example	
	.\install-win-ccmk.ps1 dev-xx.json
	<Run install setup for a given config values>

#>

[CmdLetBinding(DefaultParameterSetName="None")]
param(
	[Parameter(Mandatory=$false, Position=0, HelpMessage="Config file name to be used to populate deployment process")]      
	[string] $Config = "default.json"
)


#returns true if search pattern in registry returned only 1 value
function HlpCheckIfInstalledByName([string] $pattern){

 [string] $win64_path_machine = "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*"
 [string] $win32_path_machine = "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*"
 [string] $win64_path_user = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*"
 [string] $win32_path_user = "HKCU:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*"

  $installed = $FALSE;
  if ((Test-Path $win64_path_machine) -And $installed -eq $FALSE ){  
		$installed = ((gp $win64_path_machine).DisplayName -Match $pattern).length -ge 0
  }
  if ((Test-Path $win32_path_machine) -And $installed -eq $FALSE){
		$installed = ((gp $win32_path_machine).DisplayName -Match $pattern).length -ge 0
  }
  if ((Test-Path $win64_path_user) -And $installed -eq $FALSE){
		$installed = ((gp $win64_path_user).DisplayName -Match $pattern).length -ge 0
  }
  if ((Test-Path $win32_path_user) -And $installed -eq $FALSE){
		$installed = ((gp $win32_path_user).DisplayName -Match $pattern).length -ge 0
  }
  
  return $installed -eq $TRUE; 
}

function HlpDoWebDownload([string] $url,[string] $fname, [string] $runoptions, [bool] $run) {
	
	$path = "$PSScriptRoot\downloads"
	If(!(test-path $path)){
		Write-Host "...Download directory not found, creating"
		New-Item -ItemType Directory -Force -Path $path
	}
	
	$output = "$path\$fname"
	$start_time = Get-Date
	[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.SecurityProtocolType]::Tls12;
	$wc = New-Object System.Net.WebClient
	Write-Host "...Download started: $output"
	$wc.DownloadFile($url, $output)
	Write-Host "...Download complete"
	Write-Host "...Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"
	
	If($run){
		$proc = "$output $runoptions"
		Write-Host "...Running executable with options: $proc"
		Start-Process $output -ArgumentList $runoptions -NoNewWindow -Wait
	}
}


function DoInstallVSCode(){
	#Check if VSCode is installed, if not, install latest version
	$vs_exists = HlpCheckIfInstalledByName "Microsoft Visual Studio Code" 
	if ($vs_exists -eq $TRUE){
		Write-Host "...VS Code already installed, skipping."		
	} else{
		Write-Host "...VS Code not found,  installing."
		HlpDoWebDownload "https://aka.ms/win32-x64-user-stable" "VSCodeUserSetup.exe" "/VERYSILENT /MERGETASKS=!runcode" $TRUE	
		
		$vs_exists = HlpCheckIfInstalledByName "Microsoft Visual Studio Code.*" 
		if ($vs_exists -eq $TRUE){
			Write-Host "...VS Code installed successfully"		
		}
	}
}


function DoInstallChocolatey(){

	Write-host "...Chocolatey: $env:ChocolateyInstall"
	
	if (($env:ChocolateyInstall -eq $NULL) -OR ((Test-Path $env:ChocolateyInstall) -eq $FALSE)) {
		Write-host "...Chocolatey not found, installing."
		iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))	
	} else {
		Write-host "...Chocolatey already installed, skipping."	
	}
}

function DoInstallGitForWindows(){

	Write-Host "...Chocolatey: choco install git"
	choco install git -params '"/GitAndUnixToolsOnPath"' -y
}

function DoInstallNodejsForWindows(){

	Write-Host "...Chocolatey: choco install nodejs"
	choco install nodejs -y
}

function HlpSetVSCodeSetting([string] $setting, $value) {
	
	If((Test-Path "$env:APPDATA\Code\User") -eq $FALSE){
		Write-Host "...Code user directory not found, creating"
		New-Item -ItemType "directory" -Path "$env:APPDATA\Code\User"
	}	
	
	$fpath = "$env:APPDATA\Code\User\settings.json"
		if ((Test-Path $fpath -PathType Leaf) -eq $FALSE) {
			Write-host "...Code: Config missing, creating new one for value $setting"
			$vscode_obj = @{}
			$vscode_obj | add-member -Name $setting -Value $value -MemberType NoteProperty
			$json_string = $vscode_obj | ConvertTo-Json
			Set-Content -Path $fpath -Value $json_string
		}
		else {				
			$vscode_obj = Get-Content -Raw -Path $fpath | ConvertFrom-Json
			
			Write-host "...Code: Config present, patching for value $setting"
			$vscode_obj.PSObject.Properties.Remove($setting)			
			$vscode_obj | add-member -Name $setting -Value $value -MemberType NoteProperty
			$json_string = $vscode_obj | ConvertTo-Json 
			#present special chars from being escaped, issue with line ending like '\n'
			$json_string = [System.Text.RegularExpressions.Regex]::Unescape($json_string)
			Set-Content -Path $fpath -Value $json_string
			
		}
}

function DoInstallCodePlugins(){

	#patch config to allow self-signed certificates
	Write-host "...Code: Patching user settings to allow self-signed certificates"
	HlpSetVSCodeSetting "http.proxyStrictSSL" $FALSE
		
	#install extensions
	[string[]] $extensions = 
	"CoenraadS.bracket-pair-colorizer", 	# colorize bracket pairs
	"dbaeumer.vscode-eslint", 				#linter
	"eamodio.gitlens", 						#git integration
	"formulahendry.auto-rename-tag", 
	"geeebe.duplicate", 					#support for code line duplication
	"HookyQR.beautify", 					#autoformatting
	"pikamachu.require-cartridge-resolve", 	#allows for navigating into require synthax
	"SqrTT.prophet", 						#SFCC integration plugin
	"steoates.autoimport" 					#SFCC code support
		
	foreach ($ext in $extensions)
	{
		Write-Host "...Code: code --install-extension $ext  --force"
		code --install-extension $ext --force
	}
	
	Write-host "...Code: Patching user settings to disable self-signed certificates"
	HlpSetVSCodeSetting "http.proxyStrictSSL" $TRUE
}

function DoSetupCodeConfiguration {
	# set line endings
	Write-host "...Code: Patching user settings to use unix line-ending"
	HlpSetVSCodeSetting "files.eol" "\n"
}


function DoCloneSFRA([string] $path,[string] $path_orig, [string] $bbcred) {

	write-host "...Clone: Cleaning up provided path"
	
	
	If(test-path $path){
		Write-Host "...Clone directory found, removing"
		Remove-Item -Path $path -Recurse -Force
	}
	
	New-Item -ItemType "directory" -Path $path
	Set-Location -Path $path
	
	write-host "...Clone: Disabling http.sslVerify to work around self-signed certificate"
	git config --global http.sslVerify false
	
	#clone to local drive
	write-host "...Clone: Local path '$path'"
	
	$repo = "https://$bbcred/marykay/sfcc-commerce-cloud.git"
	git clone $repo
	
	write-host "...Clone: Switching to develop branch"
	
	Set-Location -Path $path\sfcc-commerce-cloud
	
	git fetch
	git checkout develop
	
	Set-Location -Path $path_orig
}

function DoNpmInstall([string] $path,[string] $path_orig) {

	Set-Location -Path $path\sfcc-commerce-cloud
	
	write-host "...Install: setting npm to ignore self-signed certificate in chain"
	Start-Process "C:\Program Files\Git\bin\sh.exe" -ArgumentList '--login -i -c "npm config set strict-ssl false" "%~1"' -NoNewWindow -Wait


	write-host "...Install: running npm install via bash shell $path\sfcc-commerce-cloud"
	Start-Process "C:\Program Files\Git\bin\sh.exe" -ArgumentList '--login -i -c "npm install" "%~1"' -NoNewWindow -Wait
	
	Set-Location -Path $path_orig
}


function DoCompilePackages([string] $path,[string] $path_orig) {
	write-host "...Compile: running npm compile via bash shell $path\sfcc-commerce-cloud"
	Set-Location -Path $path\sfcc-commerce-cloud
	Start-Process "C:\Program Files\Git\bin\sh.exe" -ArgumentList '--login -i -c "npm run compile:js" "%~1"' -NoNewWindow -Wait
	Start-Process "C:\Program Files\Git\bin\sh.exe" -ArgumentList '--login -i -c "npm run compile:scss" "%~1"' -NoNewWindow -Wait
	Start-Process "C:\Program Files\Git\bin\sh.exe" -ArgumentList '--login -i -c "npm run compile:svg" "%~1"' -NoNewWindow -Wait
	Set-Location -Path $path_orig
}


function DoRunDWUpload([string] $path,[string] $path_orig,[string] $shost,[string] $user,[string] $pass,[string] $codeversion,[string] $clientid,[string] $clientsecret) {
	
	write-host "...DWUpload: set up dw.json"
	#setting up dw.json file - common connection to sandbox
	$dwjson_obj =  [ordered]@{
		username = $user
		password = $pass
		hostname = $shost
		activationHostname = $shost
		"code-version" = $codeversion 
		dataBundle = "core"	
		#these are required for deployData npm scripts
		clientId = $clientid
		clientSecret = $clientsecret
	}
	
	$json_string = $dwjson_obj | ConvertTo-Json | % { [System.Text.RegularExpressions.Regex]::Unescape($_) }
	Set-Location -Path $path\sfcc-commerce-cloud
	Set-Content -Path ".\dw.json" -Value $json_string
	Set-Content -Path ".\build_tools\dw.json" -Value $json_string
	

	#do code version upload
	write-host "...DWUpload: do code version upload"
	Set-Location -Path $path\sfcc-commerce-cloud
	Start-Process "C:\Program Files\Git\bin\sh.exe" -ArgumentList '--login -i -c "npm run deployCartridges" "%~1"' -NoNewWindow -Wait
	Set-Location -Path $path_orig	
}

function DoCloneTestData([string] $path,[string] $path_orig) {
	#do data upload
	write-host "...Cloning: running npm run via bash shell $path\sfcc-commerce-cloud"
	Set-Location -Path $path\sfcc-commerce-cloud
	Start-Process "C:\Program Files\Git\bin\sh.exe" -ArgumentList '--login -i -c "npm run deployData" "%~1"' -NoNewWindow -Wait	
	Set-Location -Path $path_orig
}



function ex{exit}

#ensure that first error will halt current script execution
#$Set-StrictMode -Version 2.0
$ErrorActionPreference = "Stop"
$PSDefaultParameterValues['*:ErrorAction']='Stop'

$orig_loc = Get-Location

write-host "Version: 0.4"
write-host "Notes: This script may require manual intervention whenever automated options not available."
write-host "See Readme.md for details on recommended install configuration for different tools."
write-host ""

$config_obj = Get-Content -Raw -Path ".\$Config" | ConvertFrom-Json

#check if run as admin 
$isadmin = [bool](([System.Security.Principal.WindowsIdentity]::GetCurrent()).groups -match "S-1-5-32-544")
IF($isadmin -eq $FALSE){
	write-host "...ERROR: Unable to run commands without admin privileges. Please re-run as admin."
	write-host "...Halting script execution"
	ex
}

#install apps
if ($config_obj."skip-1" -eq $FALSE) {

	write-host ""
	write-host "1. INSTALL APPS"
	write-host "1.1 Checking VS Code install state."
	DoInstallVSCode
	write-host ""

	write-host "1.2 Checking Chocolatey install state."
	DoInstallChocolatey
	write-host ""

	write-host "1.3 Checking Git for Windows install state."
	DoInstallGitForWindows
	write-host ""

	write-host "1.4 Checking Node.js install state."
	DoInstallNodejsForWindows
	write-host ""
} else {
	write-host "1. INSTALL APPS -- skipped"
} 
#configure apps
if ($config_obj."skip-2" -eq $FALSE) {
	
	write-host ""
	write-host "2. CONFIGURE APPS"
	write-host "2.1 Installing plugins into VS Code"
	DoInstallCodePlugins
	write-host ""
	write-host "2.2 Setup VS Code configuration"
	DoSetupCodeConfiguration
	write-host ""
	
	write-host "TODO: set DW types reference and configure VS code"
	
	#todo - set up dw-api-types https://github.com/SalesforceCommerceCloud/dw-api-types
	# will require user to authenticate with https://cc-community-authmgr.herokuapp.com/

} else {
	write-host "2. CONFIGURE APPS -- skipped"
}


#clone repo
if ($config_obj."skip-3" -eq $FALSE) {	
	write-host "3. CLONE REPO"
	write-host "3.1 Clone CCMK repository into local folder"
	write-host "Note: This step may require shell restart or full machine restart in case git command is not recognized"
	DoCloneSFRA $config_obj."sfra-root" $orig_loc $config_obj."bb-cred"
	
} else {
	write-host "3. CLONE REPO -- skipped"
}

#install locally and compile 
if ($config_obj."skip-4" -eq $FALSE) {
	
	write-host ""
	write-host "4. INSTALL AND COMPILE"
	write-host "4.1 Install CCMK packages"
	DoNpmInstall $config_obj."sfra-root" $orig_loc

	write-host "4.2 Compile CCMK packages"
	DoCompilePackages $config_obj."sfra-root" $orig_loc
	
} else {
	write-host "4. INSTALL AND COMPILE -- skipped"
}

#deploy to sandbox
#NOTE! Currently this step will fail due to self-signed certificate in MSK network
if ($config_obj."skip-5" -eq $FALSE) {
	write-host ""
	write-host "5. DEPLOY TO SANDBOX"
	write-host "5.1 Deploy code to sandbox via DWUpload"
	DoRunDWUpload $config_obj."sfra-root" $orig_loc $config_obj."sandbox-host"  $config_obj."sandbox-user"  $config_obj."sandbox-pass"  $config_obj."sandbox-codeversion" $config_obj."sandbox-client-id" $config_obj."sandbox-client-secret"
	
	write-host ""
	write-host "5.2 Deploy test data to sandbox"
	DoCloneTestData $config_obj."sfra-root" $orig_loc 

} else {
	write-host "5. DEPLOY TO SANDBOX -- skipped"
}

	write-host ""
	write-host "6.1 Set secrets in sandbox" #may not be possible without approved secret storage by Infosec

	write-host ""
	write-host "7.1 Run integration tests"



#ensure orignal location is set
Set-Location -Path $orig_loc






